<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2016 Leo Feyer
 *
 * @license LGPL-3.0+
 */
/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
    'goyyamobile',
    'Helper'
));

/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    'goyyamobile\ContentReferenz'           => 'system/modules/goyyamobile/elements/ContentReferenz.php',
    'goyyamobile\ContentSlider'           => 'system/modules/goyyamobile/elements/ContentSlider.php',
    'Helper\ImageHelper'           => 'system/modules/goyyamobile/Helper/ImageHelper.php'
));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
    'ce_referenz'=>'system/modules/goyyamobile/templates',
    'ce_slider'=>'system/modules/goyyamobile/templates'
));
?>