<?php
$GLOBALS['TL_DCA']['tl_article']['palettes']['default']=str_replace('author;','author,articleSize,viewOptions,articleBackground,articleView;',$GLOBALS['TL_DCA']['tl_article']['palettes']['default']);
$GLOBALS['TL_DCA']['tl_article']['fields']['articleSize']=array(

    'label'                   => &$GLOBALS['TL_LANG']['tl_article']['articleSize'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(''=>"Kein Wrapper",'large-12'=>'100%','large-9'=>'75%', 'large-6'=>'50%'),
    'sql'                     => "varchar(10) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_article']['fields']['articleBackground']=array(

    'label'                   => &$GLOBALS['TL_LANG']['tl_article']['articleBackground'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(''=>'kein Hintergrund','green-background'=>'Grün', 'grey-background'=>'Grau','black-background'=>'Schwarz'),
    'sql'                     => "varchar(50) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_article']['fields']['articleView']=array(

    'label'                   => &$GLOBALS['TL_LANG']['tl_article']['articleView'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(''=>'------------','slider'=>'Slider','threecols'=>'3 Spaltig','iconlist'=>'Iconliste','twocols'=>'2 spaltig'),
    'sql'                     => "varchar(50) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_article']['fields']['viewOptions']=array(

    'label'                   => &$GLOBALS['TL_LANG']['tl_article']['viewOptions'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(''=>'------------','teaser'=>'Teaser anzeigen','title'=>'Titel anzeigen','both'=>'Teaser und Titel anzeigen'),
    'sql'                     => "varchar(50) NOT NULL default ''"
);