<?php
/**
 * Created by PhpStorm.
 * User: martin.wolf
 * Date: 01.07.2016
 * Time: 09:28
 */

$GLOBALS['TL_DCA']['tl_content']['palettes']['referenzliste']='{type_legend},type,headline;{Referenzen},referenz_multicolumn';
$GLOBALS['TL_DCA']['tl_content']['palettes']['slider']='{type_legend},type,headline,subheadline,buttontext,buttonlink,;{Slider Image},singleSRC,ipadBorder,showcity,designimages,background';
$GLOBALS['TL_DCA']['tl_content']['palettes']['headline']=str_replace('headline','headline,subheadline,',$GLOBALS['TL_DCA']['tl_content']['palettes']['headline']);
$GLOBALS['TL_DCA']['tl_content']['palettes']['teaser']=str_replace('article;','article;{image_legend},addImage;',$GLOBALS['TL_DCA']['tl_content']['palettes']['teaser']);
$GLOBALS['TL_DCA']['tl_content']['fields']['headline']['options']=array(
  "h1"=>"h1",
    "h2"=>"h2",
    "h3"=>"h3",
    "h4"=>"h4",
    "h5"=>"h5",
    "h6"=>"h6",
    "div.headline"=>"Kein H-Tag"

);

$GLOBALS['TL_DCA']['tl_content']['fields']['background'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['background'],
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'tl_class'=>'clr'),
    'sql'                     => "binary(16) NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['subheadline'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['subheadline'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('maxlength'=>200,'rte'=>'tinyMCE', 'helpwizard'=>true),
    'explanation'             => 'insertTags',
    'sql'                     => "mediumtext NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['ipadBorder'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['ipadBorder'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['showcity'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['showcity'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['buttontext'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['buttontext'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'options'                 => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'),
    'eval'                    => array('maxlength'=>200),
    'sql'                     => "varchar(100) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['buttonlink'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['buttonlink'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>true, 'rgxp'=>'url', 'decodeEntities'=>true, 'maxlength'=>255, 'fieldType'=>'radio', 'filesOnly'=>true, 'tl_class'=>'w50 wizard'),
    'wizard' => array
    (
        array('tl_content', 'pagePicker')
    ),
    'sql'                     => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['referenz_multicolumn']=array(
    'label'			=> &$GLOBALS['TL_LANG']['tl_content']['referenz_multicolumn'],
    'exclude' 		=> true,
    'inputType' 		=> 'multiColumnWizard',
    'eval' 			=> array
    (
        'columnFields' => array
        (
            'image' => array
            (
                'label'                 => &$GLOBALS['TL_LANG']['tl_content']['image'],
                'inputType'             => 'fileTree',
                'eval'     =>array('style' => 'width:250px','fieldType'=>'radio','files'=>true,'filesOnly'=>true,'path'=>'files/images','extensions'=>'png,jpg,jpeg,gif'),

            ),
            'hoverimage' => array
            (
                'label' 		=> &$GLOBALS['TL_LANG']['tl_content']['hoverimage'],
                'inputType' 		=> 'fileTree',
                'eval'     =>array('style' => 'width:250px','fieldType'=>'radio','files'=>true,'filesOnly'=>true,'path'=>'files/images','extensions'=>'png,jpg,jpeg,gif'),
            ),
            'name' => array
            (
                'label'                 => &$GLOBALS['TL_LANG']['tl_content']['name'],
                'exclude'               => true,
                'inputType'             => 'text',
                'eval' 			=> array('style'=>'width:180px')
            ),
            'link' => array
            (
                'label'                 => &$GLOBALS['TL_LANG']['tl_content']['link'],
                'exclude'               => true,
                'inputType'             => 'text',
                'eval'                  => array('style'=>'width:180px')

            ),

        )
    ),
    'sql'                     => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['designimages']=array(
    'label'			=> &$GLOBALS['TL_LANG']['tl_content']['designimages'],
    'exclude' 		=> true,
    'inputType' 		=> 'multiColumnWizard',
    'eval' 			=> array
    (
        'columnFields' => array
        (
            'image' => array
            (
                'label'                 => &$GLOBALS['TL_LANG']['tl_content']['designimage'],
                'inputType'             => 'fileTree',
                'eval'     =>array('style' => 'width:250px','fieldType'=>'radio','files'=>true,'filesOnly'=>true,'path'=>'files/images','extensions'=>'png,jpg,jpeg,gif'),

            ),
            'css' => array
            (
                'label'                 => &$GLOBALS['TL_LANG']['tl_content']['css'],
                'exclude'               => true,
                'inputType'             => 'textarea',
                'eval' 			=> array('style'=>'width:180px;height:40px;')
            ),


        )
    ),
    'sql'                     => "blob NULL"
);



