$(function(){

  $(document).foundation();
  var oldhtml=$("a[href*='tel:']").text();
  $("footer a[href*='tel:']").html('<i class="fa fa-phone" aria-hidden="true">' + oldhtml);

  $("[data-style!='']").each(function(){
    $(this).attr("style", $(this).attr("data-style"));
  });
  $(".slide [data-background!='']").each(function(){
    if(typeof($(this).attr("data-background"))!="undefined"){
      $(this).css({"background": "url("+$(this).attr("data-background")+")"});
    }

  });

  $("nav a[href='login.html']").parent().html("<span class='button' data-open='login'>Login</span>");

  $("#open_passwortdforgot").click(function(){
    var $modal = $('#forgot_password');
    if($modal.length==1){   //Ansonsten ist man auf passwort vergessen Seite
      $.ajax('http://www.goyyamobile.com/forgot_password.html').done(function(response){
        var parsed = $.parseHTML(response);
        result = $(parsed).find("#forgot_password").html();
        $modal.append(result).foundation('open');

      });
    }

    return false;
  });
});