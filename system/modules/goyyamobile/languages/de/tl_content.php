<?php
/**
 * Created by PhpStorm.
 * User: martin.wolf
 * Date: 01.07.2016
 * Time: 12:57
 */

$GLOBALS['TL_LANG']['tl_content']['image']=array("Referenzbild","");
$GLOBALS['TL_LANG']['tl_content']['hoverimage']=array("Hoverbild","");
$GLOBALS['TL_LANG']['tl_content']['name']=array("Name der Referenz","");
$GLOBALS['TL_LANG']['tl_content']['link']=array("Verlinkung auf Seite","");
$GLOBALS['TL_LANG']['tl_content']['background']=array("Hintergrundbild Slider");
$GLOBALS['TL_LANG']['tl_content']['subheadline']=array("Unterüberschrift");
$GLOBALS['TL_LANG']['tl_content']['ipadBorder']=array("Grenze um Bild");
$GLOBALS['TL_LANG']['tl_content']['showcity']=array("Stadt einblenden");
$GLOBALS['TL_LANG']['tl_content']['buttontext']=array("Text des Buttons");
$GLOBALS['TL_LANG']['tl_content']['buttonlink']=array("Wohin führt Button");
$GLOBALS['TL_LANG']['tl_content']['referenz_multicolumn']=array("Referenzen");
$GLOBALS['TL_LANG']['tl_content']['designimages']=array("Zusätzliche Designelemente");
