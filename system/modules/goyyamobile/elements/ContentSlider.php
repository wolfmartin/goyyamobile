<?php
/**
 * Created by PhpStorm.
 * User: martin.wolf
 * Date: 01.07.2016
 * Time: 15:07
 */

namespace goyyamobile;


use Helper\ImageHelper;

class ContentSlider extends \ContentElement
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_slider';

    /**
     * Parse the template
     *
     * @return string
     */
    public function generate(){

        if(TL_MODE=='BE'){
            $objTemplate = new \BackendTemplate('be_wildcard');
            $objTemplate->wildcard ='### GoyyaMobile  Content Element Slider Liste###';
            $objTemplate->title = $this->headline;
            $objTemplate->href='contao/main.php?do=themes&amp;table=tlmodule&amp;act=edit&amp;id='.$this->id;
            return $objTemplate->parse();
        }


        return parent::generate();
    }


    /**
     * Generate the content element
     */
    protected function compile()
    {
        $result=array();
        $helper = new \Helper\ImageHelper();
        $this->Template->headline=$this->headline;
        $this->Template->subheadline=$this->subheadline;
        $this->Template->buttontext=$this->buttontext;
        $this->Template->buttonlink=$this->buttonlink;
        $this->Template->ipadBorder=$this->ipadBorder;
        $this->Template->showcity=$this->showcity;
        $singleSRC = deserialize($this->singleSRC);
        $background = deserialize($this->background);

        $this->Template->singleSRC=$helper->getImagePath($singleSRC);
        $this->Template->background=$helper->getImagePath($background);
        $designimages = deserialize($this->designimages);
        if(!empty($designimages)) {
            foreach ($designimages as $designimage) {
                $temp=array();
                $temp['imagepath']=$helper->getImagePath($designimage['image']);
                $temp['css']=$designimage['css'];
                $result[]=$temp;
            }
        }
        $this->Template->designimages = $result;
    }

}