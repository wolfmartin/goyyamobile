<?php
namespace goyyamobile;
/**
 * Created by PhpStorm.
 * User: martin.wolf
 * Date: 01.07.2016
 * Time: 12:11
 */
class ContentReferenz extends \ContentElement{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_referenz';

    /**
     * Parse the template
     *
     * @return string
     */
    public function generate(){

        if(TL_MODE=='BE'){
            $objTemplate = new \BackendTemplate('be_wildcard');
            $objTemplate->wildcard ='### GoyyaMobile  Content Element Referenz Liste###';
            $objTemplate->title = $this->headline;
            $objTemplate->href='contao/main.php?do=themes&amp;table=tlmodule&amp;act=edit&amp;id='.$this->id;
            return $objTemplate->parse();
        }


        return parent::generate();
    }



    /**
     * Generate the content element
     */
    protected function compile()
    {
        $helper = new \Helper\ImageHelper();
        $referenzes = deserialize($this->referenz_multicolumn);
        if(!empty($referenzes)){
            foreach($referenzes as $referenz){
                $temp=array();
                $temp['image']=$helper->getImagePath($referenz['image']);
                $temp['hoverimage']=$helper->getImagePath($referenz['hoverimage']);
                $temp['name']=$referenz['name'];
                $temp['link']=$referenz['link'];
                $result[]=$temp;
            }

            $this->Template->referenzen=$result;
            $this->Template->headline=$this->headline;
        }
        return;
    }


}