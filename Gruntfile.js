/**
 * Created by martin.wolf on 30.06.2016.
 */
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';',
                stripBanners: { block: true},
            },

            src: {
                src: ['system/modules/goyyamobile/JavaScript/*.js'],
                dest: 'system/modules/goyyamobile/JavaScript/combined/<%= pkg.name %>.src.js',
            },
            lib: {

                src: ['node_modules/foundation-sites/dist/foundation.js' ],
                dest: 'system/modules/goyyamobile/JavaScript/combined/<%= pkg.name %>.lib.js',
            },
            both: {
                src: ['system/modules/goyyamobile/JavaScript/combined/<%= pkg.name %>.lib.js', 'system/modules/goyyamobile/JavaScript/combined/<%= pkg.name %>.src.js'],
                dest: 'public/<%= pkg.name %>.js',
            },
            jquery:{
                src:['node_modules/foundation-sites/node_modules/jquery/dist/jquery.min.js'],
                dest: 'public/<%= pkg.name %>.jquery.js',
            }

        },
        sass: {
            options: {                       // Target options
                sourceMap: false
            },
            dist: {
                files: {
                    'system/modules/goyyamobile/Styles/compiled/app.css':'system/modules/goyyamobile/Styles/app.scss',
                    'system/modules/goyyamobile/Styles/compiled/font-awesome.css':'system/modules/goyyamobile/Styles/scss/font-awesome.scss'

                }

            }
        },
        concat_css: {
            options: {
                // Task-specific options go here.
            },
            all: {
                src: ["system/modules/goyyamobile/Styles/compiled/*.css","node_modules/foundation-sites/dist/foundation.css"],
                dest: "public/<%= pkg.name %>.css"
            },
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true,cwd:'system/modules/goyyamobile/Fonts/', src: ['*'], dest: 'public/fonts', filter: 'isFile'},
                ],
            },
        },
        watch: {
            scripts: {
                files: ['system/modules/goyyamobile/JavaScript/*.js', 'system/modules/goyyamobile/Styles/**/*.scss'],
                tasks: ['default'],
                options: {
                    reload: true,
                    forever: true
                }
            }
        },

    });
    grunt.loadNpmTasks('grunt-contrib-concat');   // concat js files
    grunt.loadNpmTasks('grunt-concat-css');   // concat js files
    grunt.loadNpmTasks('grunt-contrib-uglify');   // concat js files
    grunt.loadNpmTasks('grunt-contrib-watch');   // concat js files
    grunt.loadNpmTasks('grunt-contrib-copy');   // concat js files
    grunt.loadNpmTasks('grunt-sass');   // concat js files
    grunt.registerTask('default', ['concat','sass','concat_css','copy','watch']);
}